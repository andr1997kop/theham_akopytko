const tabsName = document.querySelectorAll(`.tabs-title`);
const tabsParagraph = document.querySelectorAll(`.tabs-paragraph`);
const tabsPictures = document.querySelectorAll(`.tabs-pic`);

tabsName.forEach(function(elem){
    elem.addEventListener(`click`, function () {
        const currentTab = elem;
        const parId = currentTab.getAttribute(`data-par`);
        const picId = currentTab.getAttribute(`data-pic`);
        const currentPicture = document.querySelector(picId);
        const currentParagraph = document.querySelector(parId);


        tabsName.forEach(function (elem) {
            elem.classList.remove(`active`)
        });
        tabsPictures.forEach(function(elem){
            elem.classList.remove(`active-pic`)
        });
        tabsParagraph.forEach(function (elem) {
            elem.classList.remove('active-par')
        });

        currentTab.classList.add(`active`);
        currentPicture.classList.add(`active-pic`);
        currentParagraph.classList.add(`active-par`);
    })
});

const galleryOAW = document.querySelectorAll(`[data-filter]`);
const workMenu = document.querySelector(`.work-menu`);
workMenu.addEventListener(`click`, function(event){
    const t = event.target;
    if (t.getAttribute(`button-filter`) === `all`) {
        for (let i = 0; i < 12; i++) {
            galleryOAW[i].classList.remove(`hidden`);           
        }
        for (let i = 12; i < 24; i++) {
            galleryOAW[i].classList.add(`hidden`);           
        }
        loadMoreBtn.classList.remove(`hidden`);
        return;
    }
    // console.log(t.getAttribute(`button-filter`));
    for (let elem of galleryOAW) {
        // console.log(elem.getAttribute(`data-filter`));
        if (elem.getAttribute(`data-filter`) === t.getAttribute(`button-filter`)){
            elem.classList.remove(`hidden`);
        } else {
            elem.classList.add(`hidden`)
        }
    }
    loadMoreBtn.classList.add(`hidden`);
})

const loadMoreBtn = document.querySelector(`.load-more-btn`);
loadMoreBtn.addEventListener(`click`, function(event){
    galleryOAW.forEach((e)=>{
        e.classList.remove(`hidden`);
    });
    loadMoreBtn.classList.add(`hidden`);
})
console.log(galleryOAW);